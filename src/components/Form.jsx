import React from 'react';
import { useDispatch } from 'react-redux';


export default function Form(props) {
    const dispatch = useDispatch();

    const {setSinhVien,sinhVien} = props

    const handleChange = (e) => {
        const { value, name } = e.target
        setSinhVien({ ...sinhVien, [name]: value })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch({
            type: 'THEM_SINH_VIEN',
            payload: sinhVien,
        })
        setSinhVien({
            maSV: '',
            tenSV: '',
            sdt: '',
            email: '',
        })
    }

    console.log('render form')
    return (
        <div>
            <h1 className='bg-dark p-4 text-light'>Thông tin sinh viên</h1>
            <form className='container-fluid p-5' onSubmit={handleSubmit}>
                <div className='d-flex justify-content-center'>
                    <div className="me-5 flex-fill">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Mã sinh viên</label>
                            <input type="text" value={sinhVien.maSV} onChange={handleChange} name="maSV" className="form-control" />
                            <div className="form-text"></div>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputPassword1" className="form-label">Họ tên</label>
                            <input type="text" value={sinhVien.tenSV} onChange={handleChange} name="tenSV" className="form-control" />
                        </div>
                    </div>
                    <div className="ms-5 flex-fill">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Số điện thoại</label>
                            <input type="text" value={sinhVien.sdt} onChange={handleChange} name='sdt' className="form-control" />
                            <div className="form-text"></div>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputPassword1" className="form-label">Email</label>
                            <input type="text" value={sinhVien.email} onChange={handleChange} name='email' className="form-control" />
                        </div>
                    </div>
                </div>
                <button type='submit' className="btn btn-success me-4">
                    Thêm
                </button>
                <button type='submit'className="btn btn-info" onClick={()=>{
                    dispatch({
                        type:'SUA_SINH_VIEN',
                        payload:sinhVien
                    })
                }}>
                    Sửa
                </button>
            </form>

        </div>
    )
}

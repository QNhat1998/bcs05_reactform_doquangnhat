import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './table.css';

export default function Table(props) {
    const { danhSachSinhVien } = useSelector(state => state.QuanLySinhVienReducer);
    const {setSinhVien} = props;
    const dispatch = useDispatch();

    const renderTable = () => {
        return danhSachSinhVien.map((sv, index) => {
            const { maSV, email, tenSV, sdt } = sv;
            return <tr key={index}>
                <td>{maSV}</td>
                <td>{tenSV}</td>
                <td>{sdt}</td>
                <td>{email}</td>
                <td className='text-center'>
                    <button className='btn btn-danger me-4' onClick={() => {
                        console.log(maSV)
                        dispatch({
                            type: 'XOA_SINH_VIEN',
                            payload: maSV
                        })
                    }}>Xóa</button>
                    <button className='btn btn-warning' onClick={()=>{
                        setSinhVien(sv);
                    }}>Chọn</button>
                </td>
            </tr>
        })
    }

    console.log('render table')
    return (
        <table className="table-bordered table">
            <thead className='bg-dark text-light'>
                <tr>
                    <th>Mã sinh viên</th>
                    <th>Tên sinh viên</th>
                    <th>Số điện thoại</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {renderTable()}
            </tbody>
        </table>
    )
}

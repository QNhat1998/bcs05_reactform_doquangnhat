import { useState } from 'react';
import './App.css';
import Form from './components/Form';
import Table from './components/table/Table';


function App() {
  const [sinhVien, setSinhVien] = useState({maSV: '',tenSV: '',sdt: '',email: '',});
  return (
    <>
      <Form setSinhVien={setSinhVien} sinhVien={sinhVien}/>
      <Table setSinhVien={setSinhVien}/>
    </>
  );
}

export default App;

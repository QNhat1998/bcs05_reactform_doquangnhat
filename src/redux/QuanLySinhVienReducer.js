const initialState = {
    danhSachSinhVien: [],
}

export const QuanLySinhVienReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'THEM_SINH_VIEN': {
            const index = state.danhSachSinhVien.findIndex(sv => sv.maSV === payload.maSV);
            if (index === -1) {
                state.danhSachSinhVien.push(payload);
                return { ...state }
            }
            alert('Mã sinh viên tồn tại')
            return { ...state }
        }
        case 'SUA_SINH_VIEN': {
            const index = state.danhSachSinhVien.findIndex(sv => sv.maSV === payload.maSV);
            if(index !== -1){
                state.danhSachSinhVien[index] = payload;
                return { ...state }
            }
            alert('Mã sinh viên không tồn tại')
            return { ...state }
        }
        case 'XOA_SINH_VIEN': {
            const index = state.danhSachSinhVien.findIndex(sv => sv.maSV === payload);
            state.danhSachSinhVien.splice(index,1);
            alert('Xóa thành công')
            return { ...state }
        }
        default:
            return state
    }
}

import { createStore,combineReducers } from "redux";
import { QuanLySinhVienReducer } from "./QuanLySinhVienReducer";

const rootReducer = combineReducers({
    QuanLySinhVienReducer
})

export const store = createStore(rootReducer)